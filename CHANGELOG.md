## [2.4.6](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.4.5...2.4.6) (2021-03-04)


### Bug Fixes

* update dependencies ([ddb2bce](https://gitlab.com/joshua-avalon/eslint-config-react/commit/ddb2bce5d6fd4a3db473403b8d4a2971f9833810))

## [2.4.5](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.4.4...2.4.5) (2021-01-31)


### Bug Fixes

* StrictPascalCase ([4bc7e4c](https://gitlab.com/joshua-avalon/eslint-config-react/commit/4bc7e4c528aafb5cc09e3833bc97d497739a757e))

## [2.4.4](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.4.3...2.4.4) (2021-01-09)


### Bug Fixes

* __html ([91cbdc9](https://gitlab.com/joshua-avalon/eslint-config-react/commit/91cbdc9c9931666338ceb46658fa799d9dcc304c))

## [2.4.3](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.4.2...2.4.3) (2021-01-09)


### Bug Fixes

* @typescript-eslint/naming-convention ([a8649e3](https://gitlab.com/joshua-avalon/eslint-config-react/commit/a8649e398626cacfea66fec5327e3686e6d4e77e))

## [2.4.2](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.4.1...2.4.2) (2021-01-09)


### Bug Fixes

* @typescript-eslint/naming-convention ([e24623c](https://gitlab.com/joshua-avalon/eslint-config-react/commit/e24623c3c20efc8734d480ab09ea25ebc378a52a))

## [2.4.1](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.4.0...2.4.1) (2020-11-30)


### Bug Fixes

* update dependencies ([1613121](https://gitlab.com/joshua-avalon/eslint-config-react/commit/161312140edd92a75b29a05d54eb6f21219cdca5))

# [2.4.0](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.3.4...2.4.0) (2020-11-22)


### Features

* disable react-in-jsx-scope for JSX factory ([bef6691](https://gitlab.com/joshua-avalon/eslint-config-react/commit/bef669148597903d83fa10d8db4ec95aba1b170b))

## [2.3.4](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.3.3...2.3.4) (2020-11-08)


### Bug Fixes

* update dependencies ([d1b3c1e](https://gitlab.com/joshua-avalon/eslint-config-react/commit/d1b3c1edb7a42f06ecc2e934a8d37e45a81b0c82))

## [2.3.3](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.3.2...2.3.3) (2020-10-13)


### Bug Fixes

* remove react rules ([66aafce](https://gitlab.com/joshua-avalon/eslint-config-react/commit/66aafce2340d3b07fa46ec25ce686f246680d0c0))

## [2.3.2](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.3.1...2.3.2) (2020-10-12)


### Bug Fixes

* update dependencies ([81ae119](https://gitlab.com/joshua-avalon/eslint-config-react/commit/81ae119a07141138ad4605d5231b7318745ebc64))

## [2.3.1](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.3.0...2.3.1) (2020-06-29)


### Bug Fixes

* udpate dependencies ([344523f](https://gitlab.com/joshua-avalon/eslint-config-react/commit/344523f9280b218a58bdfc5e0d5bd968358156eb))

# [2.3.0](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.2.0...2.3.0) (2020-06-04)


### Features

* update dependencies ([02daa83](https://gitlab.com/joshua-avalon/eslint-config-react/commit/02daa836739a6c7ea7a78538a6afa11e6ee0c059))

# [2.2.0](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.1.0...2.2.0) (2020-04-09)


### Features

* update dependencies ([845516b](https://gitlab.com/joshua-avalon/eslint-config-react/commit/845516b8e04d6c142003e7547ec6deb5f2da8b6a))

# [2.1.0](https://gitlab.com/joshua-avalon/eslint-config-react/compare/2.0.0...2.1.0) (2020-03-17)


### Features

* update dependencies ([52a6cac](https://gitlab.com/joshua-avalon/eslint-config-react/commit/52a6cac6bc14194900f709cd768d155e8f5d517c))

# [2.0.0](https://gitlab.com/joshua-avalon/eslint-config-react/compare/1.1.2...2.0.0) (2019-08-23)


### Features

* Update dependencies ([f5d8085](https://gitlab.com/joshua-avalon/eslint-config-react/commit/f5d8085))


### BREAKING CHANGES

* Drop Node 6 support

## [1.1.2](https://gitlab.com/joshua-avalon/eslint-config-react/compare/1.1.1...1.1.2) (2019-07-31)


### Bug Fixes

* Fix overrides file ([df01c10](https://gitlab.com/joshua-avalon/eslint-config-react/commit/df01c10))

## [1.1.1](https://gitlab.com/joshua-avalon/eslint-config-react/compare/1.1.0...1.1.1) (2019-07-31)


### Bug Fixes

* Disable improper rules for React ([056efa2](https://gitlab.com/joshua-avalon/eslint-config-react/commit/056efa2))

# [1.1.0](https://gitlab.com/joshua-avalon/eslint-config-react/compare/1.0.3...1.1.0) (2019-07-22)


### Features

* Update dependencies ([35d66c0](https://gitlab.com/joshua-avalon/eslint-config-react/commit/35d66c0))
