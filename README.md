# ESLint Config React

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Shareable ESLint Typescript React config.

## Installation

```
npm i -D @joshuaavalon/eslint-config-react
```

## Usage

**.eslintrc.yaml**

```yaml
extends:
  - "@joshuaavalon/eslint-config-react"
```

### Use with VS Code

Add the following to `.vscode/settings.json` if you use ESLint extension with VS Code.

```json
{
  "eslint.autoFixOnSave": true,
  "editor.formatOnSave": true,
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    { "language": "typescript", "autoFix": true },
    { "language": "typescriptreact", "autoFix": true }
  ]
}
```

[license]: https://gitlab.com/joshua-avalon/eslint-config-react/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/joshua-avalon/eslint-config-react/pipelines
[pipelines_badge]: https://gitlab.com/joshua-avalon/eslint-config-react/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@joshuaavalon/eslint-config-react
[npm_badge]: https://img.shields.io/npm/v/@joshuaavalon/eslint-config-react/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
